<?php include('../assets/php/header.php'); ?>

<div class="row panel callout">
    <div class="small-12 columns">
        
        <h2>SiteCrafting PHP Developer Test</h2>
        
        <!--EXERCISE-->
        <a href='../index.php'>< Back</a><br />
        <h4>Exercise 3 - MySQL Models and Querying</h4>
        <br />

        <ol>
            <li>
                Using <a href="https://www.gliffy.com/" target="_blank">Gliffy</a> (we'll provide you with credentials), diagram a data model to store <b>actors</b>, <b>movies</b>, and <b>studios</b>. Feel free to use any type of diagram you feel will convey the following:
                <ul>
                    <li>An actor can be in many movies</li>
                    <li>A movie can feature many actors</li>
                    <li>Each movie is produced by a single studio</li>
                    <li>Name columns with camelCase</li>
                </ul> 
            </li>
            <li>
                Write example SQL queries to:
                <ul>
                    <li>Insert 1 studio named 'Monolith'</li>
                    <li>Insert 3 actors named
                        <ul>
                            <li>'Corey'</li>
                            <li>'Sam'</li>
                            <li>'Tracy'</li>
                        </ul>
                    </li>
                    <li>Insert 2 movies
                        <ul>
                            <li>Both are produced by studio 'Monolith'</li>
                            <li>Movie 'Accelerate' features actors 'Corey' and 'Sam'</li>
                            <li>Movie 'Velocity' features actors 'Corey' and 'Tracy'</li>
                        </ul>
                    </li>
                    <li>Use SQL to query and report the names of the actors in the movie 'Velocity'</li>
                    <li>Delete all rows from all tables to clean out test data (use delete rather than truncate)</li>
                </ul>
            </li>
            
        </ol>
        <!--END EXERCISE-->
            
    </div>
</div>

<div class="row">
    <!--ANSWERS-->
    <div class="small-12 columns">
        <pre>
            INSERT studio sample SQL here 
            <span style="color:red;"><?php echo "insert into Studio values ('Monolith');" ?></span>
        </pre>

        <pre>
            INSERT actors sample SQL here 
            <span style="color:red;"><?php echo "insert into Actors values('1','Corey','1980-08-07','American');" ?></span>
            <span style="color:red;"><?php echo "insert into Actors values('2','Sam','1990-09-17','American');" ?></span>
            <span style="color:red;"><?php echo "insert into Actors values('3','Tracy','1985-01-15','Chinese');" ?></span>
        </pre>

        <pre>
            INSERT movies sample SQL here
            <span style="color:red;"><?php echo "insert into Movies values(1,'Accelerate','Spielberg','action' ,2014,'8.4','Monolith');" ?></span>
            <span style="color:red;"><?php echo "insert into Movies values(2,'Velocity','James','fiction' ,2016,'7.4','Monolith');" ?></span>

            <span style="color:red;"><?php echo "insert into MovieActor values(1,1);" ?></span>
            <span style="color:red;"><?php echo "insert into MovieActor values(1,2); " ?></span>
            <span style="color:red;"><?php echo "insert into MovieActor values(2,1); " ?></span>
            <span style="color:red;"><?php echo "insert into MovieActor values(2,3); " ?></span>
            
        </pre>

        <pre>
            QUERY actors featured in 'Velocity' sample SQL here
            <span style="color:red;"><?php echo "select name from Actors join MovieActor on Actors.Id = MovieActor.ActorId join Movies on MovieActor.MovieId = Movies.Id where Movies.Title = 'Velocity';" ?></span> 
        </pre>

        <pre>
            DELETE all rows from tables sample SQL here
            <span style="color:red;"><?php echo "delete from Actors；" ?></span>
            <span style="color:red;"><?php echo "delete from Movies；" ?></span>
            <span style="color:red;"><?php echo "delete from MovieActor;" ?></span>
            <span style="color:red;"><?php echo "delete from Studio;" ?></span>
        </pre>
    </div>
    <!--END ANSWERS-->
</div>

<?php include('../assets/php/footer.php'); ?>
