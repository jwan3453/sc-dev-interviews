# SiteCrafting Interview Coding Challenges

To get started, clone this repo and run the application on a local server:

```
git clone git@bitbucket.org:sitecrafting/sc-dev-interviews.git
cd sc-dev-interviews
php -S localhost:9000
```

Open the above localhost address in your browser and follow the instructions there.

Good luck!

## Submitting your code

To submit your code back to us, please create a zip archive of the entire project that includes your changes. Email the zip to devint@sitecrafting.com.

Bonus points for emailing a patch or submitting a pull request instead.