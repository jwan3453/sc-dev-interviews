<?php 
include('../assets/php/header.php'); 
include('Cat.php');
include('Lion.php');
?>

<div class="row panel callout">
    <div class="small-12 columns">
        
        <h2>SiteCrafting PHP Developer Test</h2>
        
        <!--EXERCISE-->
        <a href='../index.php'>< Back</a><br />
        <h4>Exercise 2 - Object Oriented Programming</h4>
        <br />

        <ol>
            <li>Create a class called <b>Cat</b> in the file <b>02-php-oop/Cat.php</b>.
                <ul>
                    <li>Declare a protected property: <b>hungry</b></li>
                    <li>Code the constructor to accept a boolean value for <b>hungry</b> and then locally store the cat's hunger status</li>
                    <li>Code public <b>eat()</b> method to set the cat's hunger status to false</li>
                    <li>Code public <b>speak()</b> method to echo 'prrr' if the cat isn't hungry or 'MEOW!' if it is</li>
                    <li>Code public static <b>getRandomArray()</b> method to instantiate an array of 100 Cat objects. As each instance is constructed, randomly define the instance as hungry or not.</li>
                </ul>
            </li>
            <li>Create a class called <b>Lion</b> that extends Cat class in the file <b>02-php-oop/Lion.php</b>.
                <ul>
                    <li>Code member <b>speak()</b> method to echo 'roar' if the lion isn't hungry or 'ROAR!' if it is</li>
                </ul>
            </li>
            <li>In the script below, instantiate three new class objects.
                <ul>
                    <li>1 hungry cat</li>
                    <li>1 non-hungry cat</li>
                    <li>1 hungry lion</li>
                </ul>
            </li>
            <li>Make all three <b>speak()</b>. Make all three <b>eat()</b>. Make all three <b>speak()</b> again.</li>
            <li>Using static <b>getRandomArray()</b> function, generate an array of 100 cats. Find out how many are hungry and report number.</li>
        </ol>
        <!--END EXERCISE-->
            
    </div>
</div>

<div class="row">
    <div class="small-12 columns">
        
        <!--ANSWERS-->
        <?php
        //  CREATE animals
        $hungryCat = new Cat(false);
        $nonHungryCat =  new Cat(true);
        $hungryLion = new Lion(false);
        //  MAKE animals speak
        ?>
        <p>
            The first cat says: <span style="color:red;"> <?php echo $hungryCat->speak() ?> </span> <br /> 
            The second cat says: <span style="color:red;"> <?php echo $nonHungryCat->speak() ?> </span><br />
            The lion says: <span style="color:red;"> <?php echo $hungryLion->speak() ?></span><br />
        </p>
        
        <?php
        //  MAKE animals eat
            $hungryCat->eat();
            $nonHungryCat->eat();
            $hungryLion->eat();
        ?>
        <p>
            Both cats and the lion have eaten their food.
        </p>
        
        <?php
        //  MAKE animals speak again
        ?>
        <p>
            The first cat now says: <span style="color:red;"><?php echo $hungryCat->speak()?></span><br />
            The second cat now says: <span style="color:red;"><?php echo $nonHungryCat->speak()?></span><br />
            The lion now says: <span style="color:red;"><?php echo $hungryLion->speak()?></span><br />
        </p>
        
        <?php
        //  GET array of 100 cats
            $cats = Cat::getRandomArray();
        //  DETERMINE how many are hungry
            $hungryCatCounter = 0;
            $tmpHungryCat = new Cat(false);
            for( $i=0; $i<100; $i++) {
                if($cats[$i] == $tmpHungryCat) {
                    $hungryCatCounter ++;
                }
            }    
        ?>
        <p>
            Out of 100 cats, this many are currently hungry:  <span style="color:red;"><?php  echo "there are total ".$hungryCatCounter." hungry cats"; ?></span>
        </p>
        
        <!--END ANSWERS-->
            
    </div>
</div>

<?php include('../assets/php/footer.php'); ?>
