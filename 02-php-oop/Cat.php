<?php


class Cat {
  protected $hungry;
  
  function __construct($hungry) {
      if(!is_bool($hungry)){
        throw new Exception("Please ensure the value is a boolean");
      }
        
      $this->hungry = $hungry;
  }

  public function eat() {
     $this->hungry = false;
  } 

  public function speak() {
    if($this->hungry) {
      echo 'MEOW!';
    } else {
      echo 'prrr!';
    }
  }

  public static function getRandomArray() {
    $cats = [];
    $booleanList = [false, true];
    for($i=0;$i<100;$i++){
      $cats[] = new Cat($booleanList[rand(0,1)]);
    }
    return $cats;
  }
}