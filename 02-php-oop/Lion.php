<?php



class Lion extends Cat {

  function __construct( $hungry) {
    parent::__construct($hungry);
}

  public function speak() {
    if($this->hungry) {
      echo 'ROAR!';
    } else {
      echo 'roar';
    }
  }
}

