<?php include('../assets/php/header.php'); ?>

<div class="row panel callout">
    <div class="small-12 columns">
        
        <h2>SiteCrafting PHP Developer Test</h2>
        
        <!--EXERCISE-->
        <a href='../index.php'>< Back</a><br />
        <h4>Exercise 1 - PHP Tasks</h4>
        <br />

        <ol>
            <li>Define your name as a constant. Copy that contstant's value to the session. Echo that session variable's value below.</li>
            <li>Code PHP to determine the size of the SiteCrafting homepage (in bytes) and display the number in the indicated spot below. If you're spending more than 5 minutes on this, please move on and come back to it.</li>
            <li>Code the <b>echoUnlessFiddle()</b> function to echo the passed string if it doesn't contain the word 'fiddle', or throw an exception if it does.</li>
            <li>Now wrap the two calls to <b>echoUnlessFiddle()</b> function in a try/catch block so that PHP execution doesn't crash on the exception.</li>
            <li><i>Extra Credit: Using a recursive function, determine the number of files (not folders) in this repository's <b>assets</b> folder. Note: The absolute root path for this project is defined in constant ABS_ROOT.</i></li>
        </ol>
        <!--END EXERCISE-->
            
    </div>
</div>

<div class="row">
    <div class="small-12 columns">
        
        <!--ANSWERS-->
        <?php
        //  DEFINE name here
            Const name = "Jacky";
            $_SESSION['views'] = name;
        ?>
        <p>
            Your name: <?php echo $_SESSION['views'] ?>
        </p>

        
        <?php
         //DETERMINE byte size of SiteCrafting homepage here
            $content = file_get_contents("http://sitecrafting.com/"); 
            $pageSize = strlen($content);
        ?>
        <p>
            The <a href="http://www.sitecrafting.com">SiteCrafting Homepage</a> size in bytes: <span style="color:red;"> <?php echo $pageSize  ?> </span>
        </p>


        <?php
        //  DEFINE exception-throwing function here
        function echoUnlessFiddle($string)
        {
            if(strpos($string, 'fiddle') === false) {
                throw new Exception($string .' does not contain "fiddle"');
            }
            
        }

        //  TRY/CATCH code that will throw exception here
        try {
            echoUnlessFiddle('sticks!');
            echoUnlessFiddle('fiddlesticks!');
        } catch (Exception $e) {
            echo 'Error message: ' . $e->getMessage().'</br></br>';
        }   
        ?>
        <p>
            If you can see this, PHP execution wasn't crashed above.
        </p>

        
        <?php
        //  DEFINE recursive function here
            $fileCount = 0;
            function traverse($dirPath,&$fileCounter) {
                    $dir = opendir($dirPath);
                    //read files in the directory
                    while( $filename = readdir($dir)) {
                        $file = $dirPath.'/'.$filename;
                        //ignore . and ..
                        if($filename == '.' || $filename === '..'){
                            continue;
                        }
                        //if it is a dirctory
                        else if(is_dir($file)) {
                            traverse($file, $fileCounter);
                        } else {
                            $fileCounter++;
                        }
                    }
                    closedir($dir);
                    return $fileCounter;
            }
        ?>
        <p>
            The number of files in the assets folder: <span style="color:red;"> <?php echo traverse(ABS_ROOT.'/assets',$fileCount)  ?> </span>
        </p>
        <!--END ANSWERS-->
            
    </div>
</div>

<?php include('../assets/php/footer.php'); ?>
