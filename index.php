<?php include('assets/php/header.php'); ?>

<div class="row panel callout">
    <div class="small-12 columns">
        <h2>SiteCrafting PHP Developer Test</h2>
        <br />
        <h4>Things to know...</h4>
    </div>
    <div class="small-12 columns">
        <ul>
            <li>This test contains several exercises</li>
            <li>Please answer questions inline in each exercise folder's index.php file, unless instructed otherwise</li>
            <li>Test is open book; you may look things up on the internet or ask the interviewer for help on any portion of the test</li>
            <li>Use your best practices</li>
            <li>Complete as much of the test as you can within the time allotted</li>
        </ul>
    </div>
</div>

<p></p>

<div class="row">
    <div class="small-12 columns">
        <h4>Exercises</h4>
    </div>
</div>

<div class="row">
    <div class="question_set large-4 medium-12 small-12 columns">
        <p>
            <a href="01-php-tasks/">1. PHP Tasks</a>
        </p>
    </div>
    <div class="question_set large-4 medium-12 small-12 columns">
        <p>
            <a href="02-php-oop/">2. Object Oriented Programming</a>
        </p>
    </div>
    <div class="question_set large-4 medium-12 small-12 columns">
        <p>
            <a href="03-mysql/">3. MySQL Models and Querying</a>
        </p>
    </div>
</div>

<?php include('assets/php/footer.php'); ?>
