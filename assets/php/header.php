<?php

//  CONSTANTS
define("ROOT", "/"); 
define("ABS_ROOT", realpath(dirname(__FILE__)).'/../../');
    
//  SESSION
session_start();

?>
<!doctype html>
<html class="no-js" lang="en">
        <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>SiteCrafting Developer Test</title>
                <link rel="stylesheet" href="<?= ROOT ?>assets/css/foundation.css" />
                <link rel="stylesheet" href="<?= ROOT ?>assets/css/questions.css" />
                <link rel="stylesheet" href="<?= ROOT ?>assets/css/answers.css" />
                <script src="<?= ROOT ?>assets/js/vendor/modernizr.js"></script>
                <script src="<?= ROOT ?>assets/js/vendor/jquery.js"></script>
        
           <script type="text/javascript" src="//use.typekit.net/rct5alb.js"></script>
           <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        </head>
        <body>
        <br />
